# chemical

Classes, functions and data for chemistry operations.
The components can be combined to manipulate chemical systems.
See subdirectories for usage instructions.

# Chemical.Elements

* Provides a constant global variable with periodic table information:

```c++
#include "chemical/elements.hpp"
...
assert( chemical::elements["O"].atomic_mass == 15.9994*amu;
```

* Provides an `element` structure to describe elements similar to Babel.

```c++
#include "chemical/elements/element.hpp"
...
chemical::element const my_carbon_basic{
		.number                    = 6, 
		.symbol                    = "C",
		.atomic_mass               = 12.0107*amu,
		.name                      = "Carbon"
	};
```

More, optional information can be added, such as visualization color and van der Walls radius.

# Chemical.XYZ

Read, writes an manipulates XYZ information.

```c++
		auto f = xyz::frame::read(
R"XYZ(
3
comment
C         -0.469732 +0.302933 -0.845242 crystal_vector 1    5.123333  0.12      0.1222    crystal_vector 2    0.3212    5.532532  0.8931212 crystal_vector 3    0.2333    0.2211    5.22233  
C         +1.68904  +0.390523 +0.354031 crystal_origin      0.4554353 0.4324324 0.4124123
C         +1.66783  +123575   -0.801623
)XYZ"
		);
		assert( f.size() == 3 );
		assert( f.comment == "comment" );
```
