#ifdef COMPILATION_INSTRUCTIONS
(echo '#include"'$0'"'>$0.cpp)&&c++ -D_TEST_CHEMICAL_ELEMENTS -I$HOME/prj/alf $0.cpp -o$0x -lboost_serialization&&$0x;rm $0.cpp $0x;exit
#endif
// © Alfredo A. Correa 2018-2019

#ifndef CHEMISTRY_ELEMENTS_HPP
#define CHEMISTRY_ELEMENTS_HPP

#define BOOST_RESULT_OF_USE_DECLTYPE

#include "./elements/element.hpp"

#include "boost/function_constant.hpp"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/random_access_index.hpp>

namespace chemical{

using boost::multi_index_container;
using boost::multi_index::indexed_by;
using boost::multi_index::ordered_unique;
using boost::multi_index::ordered_non_unique;
using boost::multi_index::identity;
using boost::multi_index::member;
using boost::multi_index::tag;
using boost::multi_index::random_access;

using namespace boost::multi_index;

using chemical::element;

using element_t_base = multi_index_container<
	element,
	indexed_by<
		random_access<>,
		ordered_unique<tag<element::by_number>, member<element, int, &element::number>>,
		ordered_unique<tag<element::by_symbol>, boost::member_ptr_constant<std::string element::*, &element::symbol>>,
		ordered_unique<tag<element::by_name  >, member<element, std::string, &element::name>>
	>
>;

struct elements_t : element_t_base{
	using base_type = element_t_base;
	using element_t_base::operator[];
	element const& operator[](std::string const& symbol) const{
		{
			using criteria = element::by_name;
			auto it = this->get<criteria>().find(symbol);
			if(this->get<criteria>().end() != it) return *it;
		}
		{
			using criteria = element::by_symbol;
			auto it = this->get<criteria>().find(symbol);
			if(this->get<criteria>().end() != it) return *it;
		}
		throw std::domain_error("element with name/symbol \""+symbol+"\" not found");
	}
};

static elements_t const& elements = [](){
	elements_t ret; 
	ret.reserve(119);
	auto data =&true[
		#include"elements/elements.txi" // std::ifstream ifs("/usr/share/openbabel/2.4.1/element.txt"); 
	];
	std::istringstream ifs(data); assert(ifs);
	element e;
	for(std::string line; getline(ifs, line); ret.push_back(std::move(e))){
		if(line[0]=='#') continue;
		std::istringstream iss{line};
		{             iss>> e.number;}
		{             iss>> e.symbol;}
		{double aen ; iss>> aen ; if(aen  != 0.0) e.AR_electronegativity=aen;}
		{double cr  ; iss>> cr  ; if(cr   != 1.6) e.covalent_radius=cr*units::AA;}
		{double bor ; iss>> bor ; /* if(bor != 1.6) e.bond_order_radius=bor*units::AA;*/}
		{double vdwr; iss>> vdwr; if(vdwr != 2.0) e.vdW_radius = vdwr*units::AA;}
		{int mbv    ; iss>> mbv ; if(mbv  != 6  ) e.max_bond_valence = mbv;}
		{             iss>> quantity_cast<double&>(e.atomic_mass);}
		{double pen ; iss>> pen ; if(pen  != 0.0) e.Pauling_electronegativity = pen;} 
		{double ip  ; iss>> ip  ; if(ip   != 0.0) e.ionization_potential = ip*units::eV;}
		{double ea  ; iss>> ea  ; if(ea   != 0.0) e.electron_affinity = ea*units::eV;}
		{             iss>> e.red >> e.green >> e.blue;}
		{             iss>> e.name;}
	}
	assert(ret.size() >= 119);
	return ret;
}();

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef _TEST_CHEMICAL_ELEMENTS

#include<iostream>

int main(){

	std::cout << chemical::elements["O"].atomic_mass << std::endl;

}
#endif
#endif

