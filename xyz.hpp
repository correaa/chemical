#ifdef compile_instructions 
(echo '#include"'$0'"'>$0.cpp)&&c++ -D_CHEMICAL_XYZ_TEST -I$HOME/prj $0.cpp -o$0x -lstdc++fs&&$0x&&rm $0.cpp $0x;exit
#endif
#ifndef CHEMICAL_XYZ_HPP
#define CHEMICAL_XYZ_HPP
// © Alfredo A. Correa 2018-2019

#include "./xyz/frame.hpp"
#include "./xyz/io.hpp"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef _CHEMICAL_XYZ_TEST

#include<map>
#include<cassert>
#include "./xyz/io.hpp"

int main(){

	namespace xyz = chemical::xyz;
	using xyz::AA;
	{
		xyz::frame atoms_only =
			{{
				xyz::atom{"C", {1.2*AA, 3.4*AA, 5.1*AA}},
				xyz::atom{"H", {4.5*AA, 8.9*AA, 12.*AA}},
				xyz::atom{"H", {5.1*AA, 9.1*AA, 21.*AA}}
			}}
		;
		assert( atoms_only.size() == 3 );
		assert( not atoms_only.xyz::crystal::vectors[0] );
//		assert(( atoms_only.xyz::crystal::vectors[0].value_or(xyz::vector{0.*AA, 0.*AA, 0.*AA}) == xyz::vector{0.*AA, 0.*AA, 0.*AA} ));
		sort(begin(atoms_only), end(atoms_only), [](auto& a, auto& b){return a.z < b.z;});
		assert( is_sorted(begin(atoms_only), end(atoms_only), [](auto& a, auto& b){return a.z < b.z;}) );

		stable_sort(begin(atoms_only), end(atoms_only));
		auto hydrogens = equal_range(begin(atoms_only), end(atoms_only), "H");
		assert( hydrogens.first->z == 12.*AA );

		std::map<std::string, std::vector<xyz::vector>> m;
		for(auto& a: atoms_only) m[a.symbol].push_back(a);
		assert( m["H"].front().x == 4.5*AA );
		atoms_only.xyz::crystal::vectors = {{
				{{1.*AA, 0.*AA, 0.*AA}}, // crystal vectors
				{{0.*AA, 1.*AA, 0.*AA}}, 
				{{0.*AA, 0.*AA, 1.*AA}}
			}}
		;
	}
	{
		xyz::frame const atoms_crystal_3d =
			{
				{
					{"C", {1.2*AA, 3.4*AA, 5.1*AA}},
					{"H", {4.5*AA, 8.9*AA, 12.*AA}},
					{"H", {5.1*AA, 9.1*AA, 21.*AA}}
				},
				{{{
					{{1.*AA, 0.*AA, 0.*AA}}, // crystal vectors
					{{0.*AA, 1.*AA, 0.*AA}}, 
					{{0.*AA, 0.*AA, 1.*AA}}
				}}}			
			}
		;
		assert( atoms_crystal_3d.size() == 3 );
		assert( atoms_crystal_3d[2].y == 9.1*AA );
		assert( atoms_crystal_3d.xyz::crystal::vectors[1] and (*atoms_crystal_3d.xyz::crystal::vectors[1]).y == 1.*AA );
		assert( atoms_crystal_3d.xyz::crystal::vectors[2] and atoms_crystal_3d.xyz::crystal::vectors[2]->z == 1.*AA );
	}
	{
		xyz::crystal const periodic3d_3x3x3{
			.vectors = {{ 
				{{1.*AA, 0.*AA, 0.*AA}}, // crystal vectors
				{{0.*AA, 1.*AA, 0.*AA}}, 
				{{0.*AA, 0.*AA, 1.*AA}}
			}},
			.origin = {{0.*AA, 0.*AA, 0.*AA}}, // crystal origin
			.images = {{3, 3, 3}}     // images
		};
	}
	{
		auto const atoms_crystal_3d =
			xyz::frame{
				xyz::atoms{
					xyz::atom{"C", xyz::vector{1.2*AA, 3.4*AA, 5.1*AA}},
					xyz::atom{"H", xyz::vector{4.5*AA, 8.9*AA, 12.*AA}},
					xyz::atom{"H", xyz::vector{5.1*AA, 9.1*AA, 21.*AA}}
				},
				xyz::crystal{
					.vectors = {{
						{xyz::vector{1.*AA, 0.*AA, 0.*AA}}, // crystal vectors
						{xyz::vector{0.*AA, 1.*AA, 0.*AA}}, 
						{xyz::vector{0.*AA, 0.*AA, 1.*AA}}
					}}
				}
			}
		;
		assert( atoms_crystal_3d.size() == 3 );
		assert( atoms_crystal_3d[2].y == 9.1*AA );
		assert( atoms_crystal_3d.xyz::crystal::vectors[1] and (*atoms_crystal_3d.xyz::crystal::vectors[1]).y == 1.*AA );
		assert( atoms_crystal_3d.xyz::crystal::vectors[2] and atoms_crystal_3d.xyz::crystal::vectors[2]->z == 1.*AA );
	}
	{
		xyz::frame cluster;
		auto a = 1.51*AA;
		for(int i=-10; i!=10;++i) for(int j=-10; j!=10;++j) for(int k=-10; k!=10;++k) 
			cluster.push_back({"C", {a*double(i), a*double(j), a*double(k)}});
		cluster.erase(remove_if(
			begin(cluster), end(cluster), 
			[](auto&& e){return norm(e) > 25.*AA*AA;}
		), end(cluster));
		cluster.save("cluster.xyz");
	}
	{
		xyz::frame const atoms_crystal_explicit_0d =
			{
				{
					{"C", {1.2*AA, 3.4*AA, 5.1*AA}},
					{"H", {4.5*AA, 8.9*AA, 12.*AA}},
					{"H", {5.1*AA, 9.1*AA, 21.*AA}}
				},
				{{{ 
					{}, // each of the 3 crystal vectors are "empty"
					{}, 
					{}
				}}}
			}
		;
		assert( atoms_crystal_explicit_0d.size() == 3 );
		assert( not atoms_crystal_explicit_0d.xyz::crystal::vectors[0] );
	}
	{
		xyz::frame const atoms_crystal_explicit_0d =
			{
				{
					{"C", {1.2*AA, 3.4*AA, 5.1*AA}},
					{"H", {4.5*AA, 8.9*AA, 12.*AA}},
					{"H", {5.1*AA, 9.1*AA, 21.*AA}}
				},
				{} // explicitly empty crystall information
			}
		;
		assert( atoms_crystal_explicit_0d.size() == 3 );
		assert( not atoms_crystal_explicit_0d.xyz::crystal::vectors[0] );
	}
	{
		xyz::frame mutable_chain_1d;
		mutable_chain_1d.reserve(10);
		for(int i = 0; i != 10; ++i) mutable_chain_1d.push_back({"C", {i*2.1*AA, 0.*AA, 0.*AA}});
		mutable_chain_1d.xyz::crystal::vectors[0] = {{30.*AA, 0.*AA, 0.*AA}};
		mutable_chain_1d.save("chain_1d.xyz");
		assert( mutable_chain_1d[2].symbol == "C" );
	}
	{
		auto frame_from_string = xyz::frame::read(std::next
(R"XYZ(
3
comment
C         -0.469732 +0.302933 -0.845242 crystal_vector 1    5.123333  0.12      0.1222    crystal_vector 2    0.3212    5.532532  0.8931212 crystal_vector 3    0.2333    0.2211    5.22233  
C         +1.68904  +0.390523 +0.354031 crystal_origin      0.4554353 0.4324324 0.4124123
C         +1.66783  +123575   -0.801623
)XYZ")
		);
		assert( frame_from_string.size() == 3 );
		assert( frame_from_string.comment == "comment" );
	}
	{
		auto frame_from_file = xyz::frame::load("xyz/example.xyz");
		assert( frame_from_file.size() == 3 );
	}
}
#endif
#endif

