<!--
(pandoc `#--from gfm` --to html --toc --standalone --metadata title=" " $0 > $0.html) && firefox --new-window  $0.html; exit
-->

# chemical-xyz

*Alfredo A. Correa* 2018 <alfredo.correa@gmail.com>

Programatically read/write/manipulate XYZ Chemical format (as specific in https://www.nongnu.org/xmakemol/XmakemolDocumentation.html)

# Read XYZ data

From a file:

```c++
	using chemical::xyz::frame;
	auto frame_from_file = frame::load("xyz/example.xyz");
	assert( size(frame_from_file) == 3 );
```

From raw text or strings:

```c++
	auto frame_from_string = frame::read(
R"xyz(
3
comment
C         -0.469732 +0.302933 -0.845242 crystal_vector 1    5.123333  0.12      0.1222    crystal_vector 2    0.3212    5.532532  0.8931212 crystal_vector 3    0.2333    0.2211    5.22233  
C         +1.68904  +0.390523 +0.354031 crystal_origin      0.4554353 0.4324324 0.4124123
C         +1.66783  +123575   -0.801623
)xyz"
	);
	assert( size(frame_from_string) == 3 );
	assert( frame_from_string.comment == "comment" );
```

# Construct XYZ data

Atoms only, no crystal information:

```c++
	namespace xyz = chemical::xyz;
	xyz::frame const atoms_only =
		{{
			{"C", {1.2, 3.4, 5.1}},
			{"H", {4.5, 8.9, 12.}},
			{"H", {5.1, 9.1, 21.}}
		}}
	;
	assert( size(atoms_only) == 3 );
	assert( not atoms_only.crystal::vectors[0].has_value() );
```

Atoms and crystal information:

```c++
	xyz::frame const atoms_crystal_3d =
		{
			{
				{"C", {1.2, 3.4, 5.1}},
				{"H", {4.5, 8.9, 12.}},
				{"H", {5.1, 9.1, 21.}}
			},
			{{{ 
				{{1., 0., 0.}}, // crystal vectors
				{{0., 1., 0.}}, 
				{{0., 0., 1.}}
			}}}			
		}
	;
	assert( size(atoms_crystal_3d) == 3 );
	assert( atoms_crystal_3d.crystal::vectors[0].has_value() and (*atoms_crystal_3d.crystal::vectors[1]).y == 1. );
```

Some designated initializers and types can be used to improve the readability of the contructor:

```c++
	 auto const atoms_crystal_3d =
		xyz::frame{
			xyz::atoms{
				xyz::atom{"C", xyz::vector{1.2, 3.4, 5.1}},
				xyz::atom{"H", xyz::vector{4.5, 8.9, 12.}},
				xyz::atom{"H", xyz::vector{5.1, 9.1, 21.}}
			},
			xyz::crystal{
				.vectors = {{
					{xyz::vector{1., 0., 0.}}, // crystal vectors
					{xyz::vector{0., 1., 0.}}, 
					{xyz::vector{0., 0., 1.}}
				}}
			}			
		}
	;
```

# Manipulate XYZ data

Generate atoms on-the-fly, 

```c++
	xyz::frame mutable_chain_1d;
	mutable_chain_1d.reserve(10);
	for(int i = 0; i != 10; ++i) mutable_chain_1d.push_back({"C", {i*2.1, 0., 0.}});
	mutable_chain_1d.crystal::vectors[0] = {30., 0., 0.};
```

`chemical::xyz::frame`'s interface is a subset of `std::vector<chemical::xyz::atom>`'s interface.

Sort atoms by 'z'-coordinate:

```c++
	chemical::xyz::frame atoms = // non const to manipulate
		{{
			{"C", {1.2, 3.4, 5.1}}, // or {"C", 1.2, 3.4, 5.1}
			{"H", {4.5, 8.9, 12.}},
			{"H", {5.1, 9.1, 21.}}
		}}
	;
	sort(begin(atoms), end(atoms), [](auto& a, auto& b){return a.z < b.z;});
```

Sort atoms by element name *and* 'y'-coordinate:

```c++
	sort(begin(atoms), end(atoms), [](auto& a, auto& b){return std::tie(element(a), a.y) < std::tie(element(b), b.y);});
```

Sort atoms by element but preserving the initial relative order (`chemical::xyz::atoms` by default have a weak ordering on the element type and the order).

```
	stable_sort(begin(atoms), end(atoms));
```

Remove atoms of a certain element type:

```c++
	atoms.erase(remove_if(begin(atoms), end(atoms), [](auto& a){return element(a) == 'C';}));
```

Cut a spherical cluster of radius 5 from a crystalline structure:

```c++
	chemical::xyz::frame cluster;
	auto a = 1.51;
	for(int i=-10; i!=10;++i) for(int j=-10; j!=10;++j) for(int k=-10; k!=10;++k) 
		cluster.push_back({"C", {a*i, a*j, a*k}});
	cluster.erase(remove_if(
		begin(cluster), end(cluster), 
		[](auto&& e){return norm(e) > 5.*5.;}
	), end(cluster));
	cluster.save("cluster.xyz");
```


# Write XYZ data

To screen:
```c++
	using std::cout;
	cout << mutable_chain_1d; // always ends in newline
```

To a file:
```c++
	mutable_chain_1d.save("chain_1d.xyz");
```

Coordinate 'x' of all `C` atoms:

```shell
$ head chain_1d.xyz
10

C         +0        +0        +0        crystal_vector 1    30        0         0        
C         +2.1      +0        +0       
C         +4.2      +0        +0      
```

```shell
$ grep 'C ' chain_1d.xyz | cut -c 10-20 | head
 +0        
 +2.1      
 +4.2      
```

Use `cut -c 20-30` for coordinate 'y' and `cut -c 30-40` for coordinate 'z'.

# Viewing XYZ data

The library is does not visualize or interactively manipulate XYZ files, you can use other programs for that:

```bash
$ xmakemol -f cluster.xyz
$ gchem3d cluster.xyz
$ avogadro2 cluster.xyz
```



