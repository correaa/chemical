#ifdef COMPILATION_INSTRUCTIONS
clang++ -I$HOME/prj/alf $0 -o$0x -lstdc++fs &&$0x&&rm $0x; exit
#endif

#include "../../xyz.hpp"
#include "../../elements.hpp"

#include <numeric> // accumulate

#include<boost/algorithm/string.hpp> // to_lower_copy
#include<boost/units/systems/cgs/mass_density.hpp>
#include "boost/units/systems/au/si_conversion.hpp" // au to si to cgs conversion

using namespace boost::units;
namespace xyz = chemical::xyz;

int main(){
	// load xyz without crystal structure
	auto f = xyz::frame::load("136water+carbon.xyz");

	// add crystal information
	using xyz::AA;
	f.xyz::crystal::vectors[0].emplace(16.02*AA, 0.*AA, 0.*AA);
	f.xyz::crystal::vectors[1].emplace(0.*AA, 16.02*AA, 0.*AA);
	f.xyz::crystal::vectors[2].emplace(0.*AA, 0.*AA, 16.02*AA);
	
	{//	add meta information as comment, save backup with crystal information
		auto mass = accumulate(
			begin(f), end(f), chemical::mass_t{0}, 
			[](auto a, auto b){return a + chemical::elements[b.symbol].atomic_mass;}
		);

		std::ostringstream cmnt{}; cmnt<<"| volume = "<<*volume(f) <<" density = "<< quantity<cgs::mass_density>{mass/(*volume(f))};
		f.comment += cmnt.str();
		f.save("136water+carbon_crystal.xyz");
	}

	{// start output for qbox system
		std::ofstream ofs{"136water+carbon.qbs"};
		ofs<<"#"<< f.comment <<'\n'; // add comment

		// set cell
		auto in_bohr = [](auto& l){return quantity<au::length>{l}.value();};
		xyz::crystal::vector_type v[3] = {*f.vectors[0], *f.vectors[1], *f.vectors[2]};
		ofs << "set cell "
			<< in_bohr(v[0].x) <<' '<< in_bohr(v[0].y) <<' '<< in_bohr(v[0].z) <<"  "
			<< in_bohr(v[1].x) <<' '<< in_bohr(v[1].y) <<' '<< in_bohr(v[1].z) <<"  "
			<< in_bohr(v[2].x) <<' '<< in_bohr(v[2].y) <<' '<< in_bohr(v[2].z) <<'\n'
		;

		// add species
		std::set<std::string> species; for(auto& at:f) species.insert(at.symbol);
		for(auto& s : species)
			ofs<<"species "<< chemical::elements[s].name <<' '<< boost::to_lower_copy(chemical::elements[s].name)+".xml"<<'\n';
	
		// add atoms
		std::map<std::string, int> species_count;
		for(auto& at : f)
			ofs
				<<"atom "<< at.symbol << species_count[at.symbol]++ <<' '<< chemical::elements[at.symbol].name <<' '
				<< in_bohr(at.x) <<' '<< in_bohr(at.y) <<' '<< in_bohr(at.z) <<'\n'
			;
	}
}

