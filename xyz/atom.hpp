#ifdef compile_instructions 
(echo '#include"'$0'"'>$0.cpp)&&nvcc -x cu`#clang++ -Wfatal-errors` -D_TEST_CHEMICAL_XYZ_ATOM -I$HOME/prj $0.cpp -o $0x&&$0x&&rm $0.cpp $0x;exit
#endif

#ifndef CHEMICAL_XYZ_ATOM_HPP
#define CHEMICAL_XYZ_ATOM_HPP

#include "../xyz/vector.hpp"

#include "boost/units/base_units/nonsi/angstrom.hpp"

#include<string>

namespace chemical{
namespace xyz{

template<class ElementT=std::string, class Vector = xyz::vector>
struct atom_t : Vector{//: Species, Vector{
	using symbol_type = ElementT;
	using coordinate_type = Vector;
	symbol_type symbol;

	atom_t() = default;
	atom_t(symbol_type s, coordinate_type c = {}) : coordinate_type{c}, symbol{s}{}

	atom_t& operator=(atom_t const& o) = default;

	bool operator==(atom_t const& o) const{return o.symbol == symbol and static_cast<coordinate_type const&>(o) == static_cast<coordinate_type const&>(*this);}
	bool operator!=(atom_t const& o) const{return not(*this == o);}
	bool operator<(atom_t const& o) const{return symbol<o.symbol;} // partial order
	bool operator>(atom_t const& o) const{return symbol>o.symbol;}
	friend bool operator<(atom_t const& self, symbol_type s){return self.symbol < s;}
	friend bool operator<(symbol_type s, atom_t const& self){return s < self.symbol;}
};

#if __cpp_deduction_guides
template<class Species, class Vector> atom_t(Species, Vector)->atom_t<Species, Vector>;
template<class Vector> atom_t(char const*, Vector)->atom_t<std::string, Vector>;
#endif

using atom = atom_t<>;

}
}

#ifdef _TEST_CHEMICAL_XYZ_ATOM

#include<cassert>

using namespace boost::units;
namespace xyz = chemical::xyz;

int main(){

	using xyz::AA;
	{
		xyz::atom a;
	}
	{
		xyz::atom a = {"C", {1.2*AA, 3.4*AA, 6.5*AA}};
		assert( a.symbol == "C" );
		assert( a[1] == 3.4*AA );
		assert( a.symbol < "D" );
	}
	{
		xyz::atom a = {"C", {1.2*AA, 3.4*AA, 6.5*AA}};
		assert( a.symbol == "C" );
		assert( a[1] == 3.4*AA );
	}

}
#endif
#endif

