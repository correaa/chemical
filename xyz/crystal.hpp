#ifdef compile_instructions 
(echo '#include"'$0'"'>$0.cpp)&&clang++ -D_TEST_CHEMICAL_XYZ_CRYSTAL -Wfatal-errors -I$HOME/prj $0.cpp -o $0x &&$0x&&rm $0.cpp $0x; exit
#endif

#ifndef CHEMICAL_XYZ_CRYSTAL_HPP
#define CHEMICAL_XYZ_CRYSTAL_HPP

#include "../xyz/vector.hpp"

#include<array>
#include<iostream> // debug

#if not __cpp_lib_optional
#include<boost/none.hpp>
#include<boost/optional.hpp>
using boost::optional;
#else
#include<optional>
using std::optional;
#endif


namespace chemical{
namespace xyz{
	
using std::array;

#if not __cpp_lib_optional
boost::none_t const& null = boost::none;
#else
std::nullopt_t const& null = std::nullopt;
#endif

template<class Vector = xyz::vector>
struct crystal_t{
	using vector_type = Vector;
	using vectors_type = array<optional<vector_type>, 3>;
	vectors_type vectors = {};
	using origin_type = optional<vector_type>;
	origin_type origin = {};
	using images_type = optional<array<int, 3>>;
	images_type images = {};
//	crystal_t(vectors_type vectors = {}, origin_type origin = {}, images_type images = {}) 
//	: vectors{vectors}, origin{origin}, images{images}{}
	friend optional<decltype(typename vector_type::value_type{}*typename vector_type::value_type{}*typename vector_type::value_type{})> volume(crystal_t const& self){
		if(std::any_of(begin(self.vectors), end(self.vectors), [](auto& v){return not v;})) return {};
		array<vector_type, 3> vs = {*self.vectors[0], *self.vectors[1], *self.vectors[2]};
		return
			+vs[0].x*vs[1].y*vs[2].z + vs[0].y*vs[1].z*vs[2].x + vs[0].z*vs[1].x*vs[2].y
			-vs[0].z*vs[1].y*vs[2].x - vs[0].y*vs[1].x*vs[2].z - vs[0].x*vs[1].z*vs[2].y
		;
	}
};

using crystal = crystal_t<>;

}}

#ifdef _TEST_CHEMICAL_XYZ_CRYSTAL

#include<cassert>
#include<iostream>
#include<boost/units/io.hpp>

int main(){

	using chemical::xyz::crystal;
	using chemical::xyz::AA;
	{
		chemical::xyz::crystal const periodic3d_3x3x3{ 
			{{ 
				{{1.*AA, 0.*AA, 0.*AA}}, // crystal vectors
				{{0.*AA, 1.*AA, 0.*AA}}, 
				{{0.*AA, 0.*AA, 1.*AA}}
			}}, 
			{{0.*AA, 0.*AA, 0.*AA}}, // crystal origin
			{{3, 3, 3}}     // images
		};
		assert( periodic3d_3x3x3.vectors[2] and (*periodic3d_3x3x3.vectors[2]).z == 1.*AA );
		assert( periodic3d_3x3x3.vectors[1] and periodic3d_3x3x3.vectors[1]->y == 1.*AA );
		assert( volume(periodic3d_3x3x3) );
		std::cout << *volume(periodic3d_3x3x3) << std::endl;
		assert( *volume(periodic3d_3x3x3) == 1.*AA*AA*AA );
	}
	{
		chemical::xyz::crystal const periodic2d_no_images_no_origin{ 
			{{ 
				{{1.*AA, 0.*AA, 0.*AA}}, // crystal vectors
				{{0.*AA, 1.*AA, 0.*AA}},
			}} 
		};
	}
	{
		chemical::xyz::crystal const periodic3d_3x3x3{
			.vectors = {{ 
				{{1.*AA, 0.*AA, 0.*AA}}, // crystal vectors
				{{0.*AA, 1.*AA, 0.*AA}}, 
				{{0.*AA, 0.*AA, 1.*AA}}
			}},
			.origin = {{0.*AA, 0.*AA, 0.*AA}}, // crystal origin
			.images = {{3, 3, 3}}     // images
		};
		assert( periodic3d_3x3x3.vectors[2] and (*periodic3d_3x3x3.vectors[2]).z == 1.*AA );
		assert( periodic3d_3x3x3.vectors[1] and periodic3d_3x3x3.vectors[1]->y == 1.*AA );
	}
}
#endif
#endif

