#ifdef compile_instructions 
(echo '#include"'$0'"'>$0.cpp)&&c++ -D_TEST_CHEMICAL_XYZ_IO -I$HOME/prj/alf $0.cpp -o $0x -lstdc++fs&&$0x&&rm $0.cpp $0x; exit
#endif
// © Alfredo A. Correa 2018-2019

#ifndef CHEMICAL_XYZ_IO_HPP
#define CHEMICAL_XYZ_IO_HPP

#include "./frame.hpp"
#include "./parser/frame.hpp"

#include<boost/spirit/home/support/iterators/istream_iterator.hpp>
#include<boost/format.hpp>

#include<iomanip>
#include<fstream>

namespace chemical{
namespace xyz{

namespace x3 = boost::spirit::x3;

std::istream& operator>>(std::istream& is, xyz::frame& f){
	f = frame{};
	std::ios_base::fmtflags back{is.flags()};
	is.unsetf(std::ios::skipws); // No white space skipping!
	boost::spirit::istream_iterator first{is}, last;
	bool r = phrase_parse(first, last, xyz::parser::frame, x3::blank, f);
	is.flags(back);
	if(not r) is.setstate(std::ios::failbit);
	return is;
}

std::ostream& operator<<(std::ostream& os, xyz::atom const& a){
	std::ios_base::fmtflags back{os.flags()};
	os.precision(std::numeric_limits< double >::max_digits10 - 12);
	os <<std::showpos<<std::left; using std::setw;
	os 
		<<setw(9)<< a.symbol <<' '
		<< boost::format("%+9.3f")% double(a.x/xyz::AA) <<' '
		<< boost::format("%+9.3f")% double(a.y/xyz::AA) <<' '
		<< boost::format("%+9.3f")% double(a.z/xyz::AA)
	;
	os.flags(back);
	return os;
}

std::ostream& operator<<(std::ostream& os, xyz::vector const& v){
	os.precision(std::numeric_limits< double >::max_digits10 - 11);
	using std::fixed; using std::setw;
	return os<<fixed
		<<setw(9)<< xyz::vector::value_type::value_type(v.x/xyz::AA) <<' '
		<<setw(9)<< xyz::vector::value_type::value_type(v.y/xyz::AA) <<' '
		<<setw(9)<< xyz::vector::value_type::value_type(v.z/xyz::AA) <<' '
	;
}

std::ostream& operator<<(std::ostream& os, std::array<int, 3> const& v){
	using std::setw;
	return os<<setw(2)<< v[0] <<' '<< setw(2) << v[1] <<' '<< setw(2) << v[2];
}

std::ostream& operator<<(std::ostream& os, xyz::frame const& f){
	os<< f.size() <<'\n'<< f.comment <<'\n';
	using std::setw;
	using std::left;
	for(xyz::frame::size_type i = 0; i != f.size(); ++i){
		os<< f[i];
		if(i == 0){
			if(f.crystal::origin) os<<setw(21)<<" crystal_origin "<<*f.crystal::origin;
			if(f.crystal::images) os<<setw(19)<<" crystal_images "<<*f.crystal::images;
		}else if(i == 1) for(int j = 0; j != 3; ++j) 
			if(f.crystal::vectors[j]) os<<setw(20)<<left<<(" crystal_vector "+std::to_string(j+1))<<' '<<*f.crystal::vectors[j];
		os<<'\n';
	}
	return os;
}

void frame::save(path const& p) const{std::ofstream(p) << *this;}
frame frame::load(path const& p){
	frame ret;
	std::ifstream ifs{p};
	ifs >> ret;
	if(not ifs) throw std::runtime_error{"cannot read xyz frame from file '"+ p.string() +"'"};
	return ret;
}

frame frame::read(char const* s){
	frame ret;
	std::istringstream iss{std::string{s}.c_str() + (s[0]=='\n'?1:0)};
	iss >> ret;
	if(not iss) throw std::runtime_error{"cannot read xyz frame from string"};
	return ret;	
}

}}

////////////////////////////////////////////////////////////////////////////////

#ifdef _TEST_CHEMICAL_XYZ_IO

using std::cout;
namespace xyz = chemical::xyz;

int main(){
	{
		xyz::frame f;
		std::istringstream{&1[R"XYZ(
20
chemdig rzepa example_xyz Structure 6a, DFT, B3LYP, 6-31G(d), Transition mode, author C.Conesa
C -0.4697  0.3029 -0.8452  
C  1.6890  0.3905  0.3540 crystal_origin 0. 0. 0.
C  1.6678  1.3574 -0.8016
C  0.4618  1.4047 -1.3980
H -0.0018 -0.7292 -1.1224
H  2.5430  1.9817 -1.0202
H  0.0629  2.1406 -2.1001
N  2.4386 -0.7326  0.3247
O  3.4824 -0.6489 -0.3557
O  2.1007 -1.7333  1.0042
C  0.4696  0.3029  0.8452
C -1.6890  0.3905 -0.3540
C -1.6678  1.3574  0.8016
C -0.4619  1.4047  1.3980
H  0.0016 -0.7292  1.1225
H -2.5430  1.9817  1.0201
H -0.0629  2.1406  2.1001
N -2.4386 -0.7327 -0.3247
O -3.4823 -0.6489  0.3557
O -2.1006 -1.7334 -1.0041
)XYZ"]} >> f;
		cout << f.size() << std::endl;
		assert( f.size() == 20 );
		assert( f.comment == "chemdig rzepa example_xyz Structure 6a, DFT, B3LYP, 6-31G(d), Transition mode, author C.Conesa" );

		cout <<"elee "<< f[2].symbol << std::endl;
		assert( f[2].symbol == "C" );

		using xyz::AA;
		assert( f[2].y == 1.3574*AA );
		assert( !f.vectors[0] );
		cout << "---\n" << f << '\n';
	}
	{
		xyz::frame f;
		std::istringstream{&1
[R"XYZ(
3
0123456789012345678901234567890123456789012345678901234567890123456789012345678|0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
C -0.469732432  0.3029333 -0.84524233232	crystal_vector 1 5.123333 0.12 0.1222 crystal_vector 2 0.3212 5.532532 0.8931212 crystal_vector 3 0.2333 0.2211 5.22233
C  1.68904324323  0.390523323  0.354031312	crystal_origin 0.455435343 0.43243241 0.4124123121
C  1.66783432453223  12.3574666666666666666e4 -0.801623432564343
)XYZ"]
		} >> f;
		f.save("example.xyz");
		cout <<"---\n"<< f <<'\n';
		auto f2 = xyz::frame::load("example.xyz");
		f2.save("example2.xyz");
		auto f3 = xyz::frame::load("example2.xyz");
		assert( f2 == f3 );
	}
}

#endif
#endif

