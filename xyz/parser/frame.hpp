#ifdef compile_instructions 
(echo '#include"'$0'"'>$0.cpp)&&c++ -D_TEST_CHEMICAL_XYZ_PARSER -Wfatal-errors -I$HOME/prj $0.cpp -o $0x -lstdc++fs&&$0x&&rm $0.cpp $0x;exit
#endif
// © Alfredo A. Correa 2018-2019

#ifndef CHEMICAL_XYZ_PARSER_HPP
#define CHEMICAL_XYZ_PARSER_HPP

#include "../frame.hpp"
#include "../parser/atom.hpp"

#include<boost/spirit/home/x3.hpp>

namespace chemical{
namespace xyz{
namespace parser{

	namespace x3 = boost::spirit::x3;

	using x3::lit; using x3::lexeme; 
	using x3::char_; using x3::int_; using x3::double_;
	using x3::blank; using x3::eol;

	using boost::fusion::at_c;


	x3::rule<class frame_, xyz::frame> const frame = "frame";
	auto frame_def = 
		int_[([](auto&_){_val(_).reserve(_attr(_));})] > eol > 
		(lexeme[+(char_ - eol)])[([](auto&_){_val(_).comment = _attr(_);})] > eol 
		> *(
			parser::atom
			[([](auto&_){_val(_).insert(end(_val(_)), _attr(_));})] 
			>> *(
				(lit("crystal_vector") > int_ > parser::vector)
				[([](auto&_){_val(_).vectors.at(at_c<0>(_attr(_))-1)=at_c<1>(_attr(_));})]
			)
			>> -(
				(lit("crystal_origin") > parser::vector)
				[([](auto&_){_val(_).crystal::origin = _attr(_);})]
			)
			>> -(
				(lit("crystal_images") > int_>int_>int_)
				[([](auto&_){_val(_).images = crystal::images_type{{at_c<0>(_attr(_)), at_c<1>(_attr(_)), at_c<2>(_attr(_))}};})]
			)
			>> eol
		);
	BOOST_SPIRIT_DEFINE(frame)//;

}}}

////////////////////////////////////////////////////////////////////////////////

#ifdef _TEST_CHEMICAL_XYZ_PARSER

#include "../io.hpp"

namespace x3 = boost::spirit::x3;

int main(){

	namespace xyz = chemical::xyz;
	{
		std::string const str = "2\nComment\nAs 1.2\t1.3 1.4\nAs 2.1 3.1 4.1\n";
		chemical::xyz::frame XYZ;
		auto f = begin(str);
		bool r = phrase_parse(f, end(str), xyz::parser::frame, x3::blank, XYZ);

		assert(r);
		assert(XYZ.comment == "Comment");
		assert(XYZ.size() == 2);
	}
	{
		std::string const atoms_text = &1
[R"XYZ(
2
comment1 comment2
O        12.4830720891       13.1055820441        9.5288258996
O        13.1055820441       13.2222233           9.5288258996
)XYZ"];
		using xyz::AA;
		xyz::frame XYZ;
		bool r = phrase_parse(begin(atoms_text), end(atoms_text), xyz::parser::frame, x3::blank, XYZ);

		assert( XYZ.comment == "comment1 comment2" );
		assert( XYZ.size()==2 and XYZ[1].symbol=="O" and XYZ[1].y==13.2222233*AA );
		assert( not XYZ.xyz::crystal::vectors[0] );
	}
	{
		std::string const atoms_text = &1
[R"XYZ(
3
comment1 comment2
O        12.4830720891       13.1055820441        9.5288258996 crystal_origin 0.0 0.0 0.0 crystal_images 5 5 5
O        13.1055820441       13.2222233           9.5288258996 crystal_vector 1 1.2 3.4 7.5
H        1.2                 2.4                  4.5
)XYZ"];
		xyz::frame XYZ;
		using xyz::AA;
		bool r = phrase_parse(begin(atoms_text), end(atoms_text), xyz::parser::frame, x3::blank, XYZ);

		assert( XYZ.comment == "comment1 comment2" );
		assert( XYZ.size()==3 and XYZ[1].symbol=="O" and XYZ[1].y==13.2222233*AA );
		assert( XYZ.chemical::xyz::crystal::vectors[0] and XYZ.chemical::xyz::crystal::vectors[0]->x==1.2*AA);
		assert( XYZ.chemical::xyz::crystal::images and (*XYZ.chemical::xyz::crystal::images)[0] == 5 );
	}
	{
		std::string atoms_text = &1
[R"XYZ(
20
chemdig rzepa example_xyz Structure 6a, DFT, B3LYP, 6-31G(d), Transition mode, author C.Conesa
C -0.4697  0.3029 -0.8452 
C  1.6890  0.3905  0.3540 crystal_vector 1 5. 0. 0. crystal_vector 2 0. 5. 0. crystal_vector 3 0. 0. 5.
C  1.6678  1.3574 -0.8016
C  0.4618  1.4047 -1.3980
H -0.0018 -0.7292 -1.1224
H  2.5430  1.9817 -1.0202
H  0.0629  2.1406 -2.1001
N  2.4386 -0.7326  0.3247
O  3.4824 -0.6489 -0.3557
O  2.1007 -1.7333  1.0042
C  0.4696  0.3029  0.8452
C -1.6890  0.3905 -0.3540
C -1.6678  1.3574  0.8016
C -0.4619  1.4047  1.3980
H  0.0016 -0.7292  1.1225
H -2.5430  1.9817  1.0201
H -0.0629  2.1406  2.1001
N -2.4386 -0.7327 -0.3247
O -3.4823 -0.6489  0.3557
O -2.1006 -1.7334 -1.0041
)XYZ"];
		xyz::frame XYZ;
		using xyz::AA;
		bool r = phrase_parse(begin(atoms_text), end(atoms_text), xyz::parser::frame, x3::blank, XYZ);

		assert( XYZ.size() == 20 );
		assert( XYZ.comment == "chemdig rzepa example_xyz Structure 6a, DFT, B3LYP, 6-31G(d), Transition mode, author C.Conesa" );
		assert( XYZ[2].symbol=="C" and XYZ[2].y==1.3574*AA );
		assert( XYZ.xyz::crystal::vectors[0] );

		XYZ.comment = "xmakemol $@ -f $0; exit";
		XYZ.save("parser.xyz");
	}
}

#endif
#endif

