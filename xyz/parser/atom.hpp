#ifdef compile_instructions 
(echo '#include"'$0'"'>$0.cpp)&&c++ -D_TEST_CHEMICAL_XYZ_PARSER_ATOM -I$HOME/prj $0.cpp -o $0x&&$0x&&rm $0.cpp $0x;exit
#endif

#ifndef CHEMICAL_XYZ_PARSER_ATOM_HPP
#define CHEMICAL_XYZ_PARSER_ATOM_HPP

#include "../../xyz/atom.hpp"
#include "../../xyz/parser/vector.hpp"

#include<boost/spirit/home/x3.hpp>

#include <boost/fusion/algorithm/auxiliary/copy.hpp>

namespace chemical{
namespace xyz{
namespace parser{

namespace x3 = boost::spirit::x3;

using x3::char_;
using x3::double_;
using x3::lexeme;
using x3::blank;

using boost::fusion::at_c;

x3::rule<class atom_, xyz::atom> const atom = "atom";
auto atom_def = (lexeme[+(char_ - blank)] > parser::vector)
	[([](auto&_){_val(_) = xyz::atom(at_c<0>(_attr(_)), at_c<1>(_attr(_)));})];
BOOST_SPIRIT_DEFINE(atom)//;

}}}

////////////////////////////////////////////////////////////////////////////////

#ifdef _TEST_CHEMICAL_XYZ_PARSER_ATOM

namespace x3 = boost::spirit::x3;

int main(){

using chemical::xyz::AA;

{
	std::string str = "1.2\t1.3       1.4";
	chemical::xyz::vector v;
	auto f = begin(str);
	bool r = phrase_parse(f, end(str), chemical::xyz::parser::vector, x3::blank, v);
	assert( r and f==end(str) and v.z == 1.4*AA );
}

{
	std::string str = "As 1.2\t1.3 1.4";
	chemical::xyz::atom at;
	auto f = begin(str);
	bool r = phrase_parse(f, end(str), chemical::xyz::parser::atom, x3::blank, at);
	assert(r and f==end(str) );
	assert( at.symbol == "As" );
	assert( at.z == 1.4*AA );
}

}

#endif
#endif

