#ifdef compile_instructions 
(echo '#include"'$0'"'>$0.cpp)&&c++ -D_TEST_CHEMICAL_XYZ_PARSER_VECTOR -Wfatal-errors -I$HOME/prj $0.cpp -o $0x&&$0x&&rm $0.cpp $0x;exit
#endif

#ifndef CHEMICAL_XYZ_PARSER_VECTOR_HPP
#define CHEMICAL_XYZ_PARSER_VECTOR_HPP

#include "../../xyz/vector.hpp"

#include<boost/spirit/home/x3.hpp>
#include <boost/fusion/functional/invocation/invoke.hpp>

namespace chemical{
namespace xyz{
namespace parser{

namespace x3 = boost::spirit::x3;

using x3::double_;

template<class T, class Seq>
auto make(Seq&& seq){
	return invoke([](auto const&... x){return T(x...);}, seq);
}

x3::rule<class vector_, xyz::vector> const vector = "vector";
auto vector_def=(double_>double_>double_)[([](auto&_){_val(_)=make<xyz::vector_t<>>(_attr(_))*AA;})];
BOOST_SPIRIT_DEFINE(vector);

}}}

////////////////////////////////////////////////////////////////////////////////

#ifdef _TEST_CHEMICAL_XYZ_PARSER_VECTOR

namespace x3 = boost::spirit::x3;

int main(){

using chemical::xyz::AA;
{
	std::string str = "1.2\t1.3       1.4";
	chemical::xyz::vector v;
	auto f = begin(str);
	bool r = phrase_parse(f, end(str), chemical::xyz::parser::vector, x3::blank, v);
	assert(r and f==end(str) and v.z == 1.4*AA);
}

}

#endif
#endif

