#ifdef COMPILATION// -*-indent-tabs-mode:t;c-basic-offset:4;tab-width:4;-*-
$CXX -std=c++14 -I$HOME/prj/alf $0 -o $0x&&$0x&&rm $0x;exit
#endif
// © Alfredo A. Correa 2018-2020

#ifndef CHEMICAL_XYZ_VECTOR_HPP
#define CHEMICAL_XYZ_VECTOR_HPP

#include<boost/units/systems/si/dimensionless.hpp>
#include "boost/units/base_units/nonsi/angstrom.hpp"

namespace chemical{
namespace xyz{

using namespace boost::units;

template<class U = si::dimensionless, class V = double, class Q = std::conditional_t<std::is_same<U, si::dimensionless>{}, V, quantity<U, V>>>
struct vector_t;

template<class T>
vector_t<typename T::unit_type, typename T::value_type> make_vector(T const& x, T const& y, T const& z){return {x, y, z};}

template<class U, class V, class Q>
struct vector_t{
	using value_type = Q;
	union{
		std::array<value_type, 3> xyz;
		struct{value_type x, y, z;};
	};
	vector_t() : xyz{}{}
	vector_t(value_type x, value_type y, value_type z) : x{x}, y{y}, z{z}{}
	vector_t(vector_t const& o) : xyz{o.xyz}{}
	vector_t& operator=(vector_t const& o){xyz = o.xyz; return *this;}
	bool operator==(vector_t const& o) const{return o.xyz == xyz;}
	bool operator!=(vector_t const& o) const{return o.xyz != xyz;}
	decltype(auto) operator[](std::size_t s) const&{return xyz[s];}
	decltype(auto) operator[](std::size_t s)      &{return xyz[s];}
	decltype(auto) operator[](std::size_t s)     &&{return std::move(xyz[s]);}
	template<class U2, class V2 = V> using rebind = vector_t<U2, V2>;
	template<class Q2> static rebind<typename Q2::unit_type, typename Q2::value_type> remake(Q2 x, Q2 y, Q2 z){return {x, y, z};}
	friend vector_t operator+(vector_t const& s, vector_t const& o){return {s.x+o.x, s.y+o.y, s.z+o.z};}
#define FWD2 std::forward<U2>(u2)
	template<class U2> friend auto operator*(vector_t const& s, U2&& u2){return remake(s.x*FWD2, s.y*FWD2, s.z*FWD2);}
	template<class U2> friend auto operator*(vector_t      & s, U2&& u2){return remake(s.x*FWD2, s.y*FWD2, s.z*FWD2);}
	template<class U2> friend auto operator*(vector_t     && s, U2&& u2){return remake(std::move(s.x)*FWD2, std::move(s.y)*FWD2, std::move(s.z)*FWD2);}
	template<class U2> friend auto operator/(vector_t const& s, U2&& u2){return remake(s.x/FWD2, s.y/FWD2, s.z/FWD2);}
	template<class U2> friend auto operator/(vector_t      & s, U2&& u2){return remake(s.x/FWD2, s.y/FWD2, s.z/FWD2);}
	template<class U2> friend auto operator/(vector_t     && s, U2&& u2){return remake(std::move(s.x)/FWD2, std::move(s.y)/FWD2, std::move(s.z)/FWD2);}
#undef FWD2
	friend auto norm(vector_t const& s){return s.x*s.x + s.y*s.y + s.z*s.z;}
};

#if __cpp_deduction_guides
template<class Q> vector_t(Q x, Q y, Q z)->vector_t<std::conditional_t<is_dimensionless<Q>::value, si::dimensionless, typename Q::unit_type>, typename Q::value_type>;
#endif

using vector = vector_t<nonsi::length>;

template<class Vector>
auto volume(std::array<Vector, 3> a){
	return a[0].z*a[1].y*a[2].x - a[0].y*a[1].z*a[2].x - a[0].z*a[1].x*a[2].y + a[0].x*a[1].z*a[2].y + a[0].y*a[1].x*a[2].z - a[0].x*a[1].y*a[2].z;
}

using dimensionless = si::dimensionless;
auto const AA = nonsi::angstrom;

}}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if not __INCLUDE_LEVEL__ // def _TEST_CHEMICAL_XYZ_VECTOR

#include<boost/units/io.hpp>

#include<cassert>
#include<iostream>

#include<experimental/tuple>
#include<tuple>

#include "../alf/boost/multi/array.hpp"

namespace xyz = chemical::xyz;
namespace multi = boost::multi;

int f(int x, int y, int z){return x + y + z;}

template<typename T>
using xavector = chemical::xyz::vector_t<chemical::xyz::dimensionless, T>;

int main(){

	xavector<int> xav1;
	xav1.x = 1;
	xav1.y = 2;
	xav1.z = 3;

	xavector<int> xav2(2, 3, 4);

	auto xav3 = xav1 + xav2;

	using std::experimental::apply;
	assert( apply(f, xav3.xyz) == 3 + 5 + 7 ); 

	multi::array<double, 3> M(xav3.xyz, 99.);

	using xyz::AA;
	using xyz::dimensionless;

	xyz::vector v = {1.*AA, 2.*AA, 3.*AA};
	assert( v.x == 1.*AA );
	assert( v.xyz[0] == 1.*AA );
	xyz::vector w; w = v;
	assert( v == w );
	{
		xyz::vector v = {1.*AA, 2.*AA, 3.*AA}; (void)v;
	}
}
#endif
#endif

