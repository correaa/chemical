#ifdef compile_instructions
(echo '#include"'$0'"'>$0.cpp)&&nvcc -x cu `#-Wall -Wextra -Wpedantic -Wfatal-errors` -D_BOOST_NILLABLE_TEST $0.cpp -o $0x -lboost_serialization &&$0x&&rm $0x $0.cpp; exit
#endif
#ifndef BOOST_NILLABLE_HPP
#define BOOST_NILLABLE_HPP
// © Alfredo A. Correa 2018-2019

// The boost::nullable<T> name is taken already in boost Ptr Container: http://www.boost.org/doc/libs/1_59_0/libs/ptr_container/doc/reference.html#class-nullable

#include<boost/lexical_cast.hpp>
#include<boost/type_index.hpp>

//#include<optional>
#include<boost/optional.hpp>
#include<boost/serialization/optional.hpp>

namespace boost{ // boost::nullable<T> name is taken already by Boost Ptr Container

struct bad_nillable_cast : std::bad_cast{
	std::string what_;
	bad_nillable_cast(std::string w) : what_(w){}
	const char* what() const noexcept override{return what_.c_str();}
};

struct bad_nil_compare : std::domain_error{
	using std::domain_error::domain_error;
};

struct bad_nil_equality : std::domain_error{
	using std::domain_error::domain_error;
};

//using std::optional;
//using ::boost::optional; // not necessary in current namespace nvcc warning
using nullopt_t = boost::none_t;
static nullopt_t const nullopt = boost::none;

using nil_t = nullopt_t;
static nil_t const nil = nullopt;

template<class T>
[[noreturn]] T throw_bad_nillable_cast(){
	throw bad_nillable_cast{"cannot convert nil to "+ boost::typeindex::type_id<T>().pretty_name()};
}

template<class T, T(Value)() = throw_bad_nillable_cast<T>> struct nillable;

template<class T, T(Value)()>
struct nillable : 
	private optional<T>, 
	private boost::less_than_comparable<nillable<T>>,
	private boost::equality_comparable<nillable<T>>
{
	static nil_t const nil;// = boost::nil;
private:
	using Base = optional<T>;
	optional<T>& to_base(){return static_cast<optional<T>&>(*this);}
	Base const& to_base() const{return static_cast<optional<T> const&>(*this);}
	Base& base_cast(){return static_cast<Base&>(*this);}
public:
	using value_type = T;
	using optional<T>::optional;
	using optional<T>::value_or;
	nillable() : optional<T>(T()){}
	nillable(nil_t) : optional<T>(nullopt){}
	template<class... Args> 
	value_type value_or(Args&&... args) const{
		if(static_cast<optional<T> const&>(*this)) return value();
		return value_type(std::forward<Args>(args)...);
	}
	friend value_type operator,(value_type const& t, nillable const& s){
		// activate -Wno-unused-value to avoid warning in generic code
		if(s.has_value()) return s.value();
		return t;
	}
	friend nillable const& operator,(nillable const& s, nillable const& o){
		return o.has_value()?o:s;
	}
	bool has_value() const{return (bool)(to_base());}
	public:
	optional<T> const& operator&() const{return to_base();}
	optional<T>& operator&(){return to_base();}
	template<class TT> operator TT() const{return value();}
	decltype(auto) value() const{
		if(not static_cast<optional<T> const&>(*this)) return Value();//throw bad_nullable_cast{"cannot convert null value to " +  boost::typeindex::type_id<T>().pretty_name()};
		return decltype(Value())(*(*this));
	}
	friend std::ostream& operator<<(std::ostream& os, nillable const& self){
		if(self.has_value()) return os<< static_cast<T const&>(self);
		return os<<"--";
	}
	friend std::istream& operator>>(std::istream& is, nillable& s){
		std::string input; is >> input;
		if(input == "--") s = boost::nil;
		else s = boost::lexical_cast<T>(input);
		return is;
	}
	friend auto operator<(nillable const& n1, nillable const& n2) -> decltype(std::declval<T>()<std::declval<T>()) try{
		return n1.value()<n2.value();
	}catch(bad_nillable_cast const& t){throw bad_nil_compare("nil is not comparable");}
	friend auto operator==(nillable const& n1, nillable const& n2) -> decltype(std::declval<T>()==std::declval<T>()) try{
		return n1.value()==n2.value();
	}catch(bad_nillable_cast const& t){throw bad_nil_equality("nil is not equality comparable");}
	friend auto operator==(nillable const& n1, T const& n2) -> decltype(std::declval<T>()==std::declval<T>()) try{
		return n1.value()==n2;
	}catch(bad_nillable_cast const& t){throw bad_nil_equality("nil is not equality comparable");}

	template<class Archive>//, class T>
	inline void serialize(Archive& archive, int){//boost::nullable<T>& nbl, const unsigned int /*version*/){
		optional<T>& optional = *this;
		archive & boost::serialization::make_nvp("", optional);
	}
};

template<class T> nillable<T> make_nillable(T const& t){return {t};}

template<class T>
struct unknowable : nillable<T>{
	using nillable<T>::nillable;
	friend auto operator<(unknowable<T> const& n1, unknowable<T> const& n2) -> unknowable<decltype(std::declval<T>() < std::declval<T>())>{
		if(not &n1 or not &n2) return {};
		return static_cast<T const&>(n1) < static_cast<T const&>(n2);
	}
	friend auto operator==(unknowable const& n1, unknowable const& n2) -> unknowable<decltype(std::declval<T>() == std::declval<T>())>{
		if(not &n1 or not &n2) return {};
		return n1.value() == n2.value();
	}
	friend auto operator==(unknowable const& n1, T const& n2) -> unknowable<decltype(std::declval<T>() == std::declval<T>())>{
		if(not &n1) return {};
		return n1.value() == n2;
	}
};

template<class T> static unknowable<T> unknown = unknowable<T>{};

template<class T>
struct ordered_nillable : 
	public  nillable<T>,
	private boost::less_than_comparable<ordered_nillable<T>>,
	private boost::equality_comparable<ordered_nillable<T>>
{
	using nillable<T>::nillable;

	friend auto operator<(ordered_nillable<T> const& n1, ordered_nillable<T> const& n2) -> decltype(std::declval<T>() < std::declval<T>()){
		if(not n1.has_value() and n2.has_value()) return true;
		if(    n1.has_value() and not n2.has_value()) return false;
		if(not n1.has_value() and not n2.has_value()) return false;
		return static_cast<T const&>(n1) < static_cast<T const&>(n2);
	}
	friend auto operator<(ordered_nillable const& n1, T const& n2) -> decltype(std::declval<T>() < std::declval<T>()){
		if(not n1.has_value()) return true;
		return static_cast<T const&>(n1) < static_cast<T const&>(n2);
	}
	friend auto operator==(ordered_nillable const& n1, ordered_nillable const& n2) -> decltype(std::declval<T>() == std::declval<T>())
	{
		if(not n1.has_value() and not n2.has_value()) throw bad_nil_equality("two ordered null values are not equality comparable");
		if(not n1.has_value() and n2.has_value()) return false;
		if(    n1.has_value() and not n2.has_value()) return false;
		return n1.value() == n2.value();
	}
};

template<class T>
ordered_nillable<T> make_ordered_nillable(T const& t){return {t};}

template<class T>
struct regular_nillable : 
	public  ordered_nillable<T>,
	private boost::less_than_comparable<regular_nillable<T>>,
	private boost::equality_comparable<regular_nillable<T>>
{
	using ordered_nillable<T>::ordered_nillable;
	friend auto operator==(regular_nillable const& n1, regular_nillable const& n2) -> decltype(std::declval<T>() == std::declval<T>())
	{
		if(not n1.has_value() and not n2.has_value()) return true;
		if(not n1.has_value() and n2.has_value()) return false;
		if(    n1.has_value() and not n2.has_value()) return false;
		return n1.value() == n2.value();
	}

};

template<class T>
regular_nillable<T> make_ordered_nillable(T const& t){return {t};}

//template<class T = void> static constexpr nillable<T> nil = {};
//template<> constexpr nil_t nil<void> = nil;

using ndouble   = nillable<double>;
using nint      = nillable<int>;
using nunsigned = nillable<unsigned>;
using nlong     = nillable<long>;

using ondouble   = ordered_nillable<double>;
using onint      = ordered_nillable<int>;
using onunsigned = ordered_nillable<unsigned>;
using onlong     = ordered_nillable<long>;

}

#endif

#ifdef _BOOST_NILLABLE_TEST

#include<iostream>
#include<sstream>
#include<fstream>
#include<map>

#include<boost/optional.hpp>
#include<boost/archive/text_oarchive.hpp>
#include<boost/archive/text_iarchive.hpp>

#include<boost/mpl/min_max.hpp>

//#include "/home/correaa/prj/alf/boost/archive/yml_oarchive.hpp"

using std::cout;

int main(){
#pragma GCC diagnostic ignored "-Wunused-value"
#pragma GCC diagnostic push
	{
		double a = 3.;
		cout<< (1., a) <<std::endl; // still gives warning in nvcc
	}
#pragma GCC diagnostic pop
	{
		double a = 20.;
		assert( a == 20. );
	}
	{
		boost::nillable<double> a;
		assert( (1., a) != 1. );
	}
	{
		boost::nillable<double> a = 3;
		assert( (1., a) == 3 );
	}
	{
		boost::nillable<double> a = 3;
		assert( (a, 1.) == 1. );
	}
	{
		boost::nillable<double> a = boost::nil;
		boost::nillable<double> b = 3;
		assert( (1.,a) == 1.);
		assert( (a,b) == boost::nillable<double>(3.) );
		assert( (b,a) == boost::nillable<double>(3.) );
	}
	{
//	std::experimental::optional<double> opt(5.);
	boost::nillable<double> opt = 5;

    std::stringstream ss;
	std::cout << "opt = " << opt << '\n';
	
	boost::archive::text_oarchive oa(ss);
	oa<< boost::serialization::make_nvp("opt", opt);

#if 1
    boost::archive::text_iarchive ia(ss);
 
 	boost::nillable<double> opt2;       
	ia >> boost::serialization::make_nvp("opt", opt2);
	}
	boost::nillable<double> x;
	cout<< x <<std::endl;
	boost::nillable<double> y = 5;
	cout<< y <<std::endl;
	boost::nillable<double> z = 5;
	z = boost::nil;//std::nullopt;
	cout<< z << std::endl;
	boost::nillable<double> w = 5;

	assert( w.value() == w.value() );
//	assert(5. < w);
//	assert(y < w);

//	assert(optional<double>() < optional<double>(6.));
//	assert(optional<double>(5.) < optional<double>(6.));
//	assert(std::experimental::optional<double>(7.) < std::experimental::optional<double>(6.));
//	assert(optional<double>() == optional<double>());
	
//	assert(std::make_ordered_nullable(z) < w);
//	assert(w < z);
//	w = boost::nullable<double>();
	std::cout << "w = " << w << std::endl;
//	w = std::null<>;
	{
	//	double w = 5.;
		std::cout << "(2, w) = " << (2, w) << std::endl;
	}
//	std::cout << w + 1. << std::endl;

//	auto p = std::null<double>;

//	std::map<std::experimental::optional<double>, std::string> m = {{1.,"uno"}, {2., "dos"}, {{}, "null?"}};
//	m.insert(std::make_pair(boost::optional<double>(), "null!"));
//	m[std::experimental::optional<double>()] = "nullo";
	std::map<boost::ondouble, std::string> m = {{1.,"uno"}, {2., "dos"}, {{}, "null?"}};
	m.emplace(1., "one");
//	assert( std::nullable<double>() == std::nullable<double>() );
//	m[boost::nullable<double>()] = "nullo";
	for(auto& e : m) std::cout << " " << " " << e.second << std::endl;
#endif
}
#endif

