#ifdef compile_instructions
(echo '#include"'$0'"'>$0.cpp)&&c++ -D_TEST_CHEMISTRY_ELEMENT -I$HOME/prj/alf $0.cpp -o $0x -lboost_serialization&&$0x;rm $0.cpp $0x;exit
#endif
#ifndef CHEMISTRY_ELEMENT_HPP
#define CHEMISTRY_ELEMENT_HPP
// © Alfredo A. Correa 2018-2019

#include "./nillable.hpp"

#include <boost/units/base_units/nonsi/angstrom.hpp>
#include <boost/units/base_units/nonsi/dalton.hpp>
#include <boost/units/base_units/nonsi/electron_volt.hpp>

#include<boost/serialization/nvp.hpp>

namespace chemical{

using boost::nillable;

using namespace boost::units;

namespace units{
	constexpr auto& AA = nonsi::angstrom;
	constexpr auto& eV = nonsi::electron_volt;
	constexpr auto& amu = nonsi::amu;
	constexpr auto& dalton = nonsi::dalton;
}

using radius_t = decltype(1.*units::AA);
using energy_t = decltype(1.*units::eV);
using mass_t = decltype(1.*units::amu);

inline double default_AR_electronegativity(){return 0.00;}
inline auto default_radius(){return 1.6*units::AA;}
inline auto default_energy(){return 0.*units::eV;}

struct element{
	int                                            number; struct by_number{};
	std::string                                    symbol; struct by_symbol{};
	nillable<double, default_AR_electronegativity> AR_electronegativity;
	nillable<radius_t, default_radius>             covalent_radius;
//	nullable<decltype(1.6*nonsi::angstrom)>        bond_order_radius;
	nillable<radius_t, default_radius>             vdW_radius;
	nillable<int>                                  max_bond_valence;
	mass_t                                         atomic_mass;
	nillable<double>                               Pauling_electronegativity; 
	nillable<energy_t, default_energy>             ionization_potential;
	nillable<energy_t, default_energy>             electron_affinity;
	double                                         red, green, blue;
	std::string                                    name; struct by_name{};
	element() = default;
	friend std::ostream& operator<<(std::ostream& os, element const& e){
		return os
			<<"#Num    "         <<"Symb    "         <<"ARENeg  "                     <<"RCov    "               <<"RBO     "                <<"RVdW    "            <<"MaxBnd  "                    <<"Mass    "         <<"ElNeg.  "                          <<"Ionization      "                  <<"ElAffinity      "               <<"Red     "     <<"Green   "       <<"Blue    "      <<"Name" <<'\n'
			<< e.number<<"      "<< e.symbol<<"      "<< e.AR_electronegativity<<"     "<<e.covalent_radius<<"    "<<e.covalent_radius<<"    "<< e.vdW_radius<<"     "<< e.max_bond_valence<<"       "<< e.atomic_mass<<" "<< e.Pauling_electronegativity<<"    "<< e.ionization_potential<<"         "<< e.electron_affinity<<"         "<< e.red<<"     "<< e.green<<"     "<< e.blue<<"     "<< e.name<<'\n'
		;
	}
	template<class Ar> void serialize(Ar& ar, int){
#define NVP BOOST_SERIALIZATION_NVP
		ar 
			& NVP(number)
			& NVP(symbol)
			& NVP(AR_electronegativity)
			& NVP(covalent_radius) // & NVP(bond_order_radius)
			& NVP(max_bond_valence)
			& NVP(atomic_mass)
			& NVP(Pauling_electronegativity)
			& NVP(ionization_potential)
			& NVP(electron_affinity)
			& NVP(red) & NVP(green) & NVP(blue)
			& NVP(name)
		;
#undef NVP
	}
};

}

#ifdef _TEST_CHEMISTRY_ELEMENT

#include<boost/archive/xml_oarchive.hpp>
#include<boost/archive/yml/yml_oarchive.hpp>

using std::cout; 
using namespace boost::units;

int main(){

	using chemical::units::AA;
	using chemical::units::amu;
	using chemical::units::eV;

	chemical::element const my_carbon{
		.number                    = 6, 
		.symbol                    = "C",
		.AR_electronegativity      = 2.5,
		.covalent_radius           = 0.76*AA,
		.vdW_radius                = 1.70*AA,
		.max_bond_valence          = 4,
		.atomic_mass               = 12.0107*amu,
		.Pauling_electronegativity = 2.55,
		.ionization_potential      = 11.2603*eV,
		.electron_affinity         = 1.262118*eV,
		.red = 0.40, .green = 0.40, .blue = 0.40,
		.name                      = "Carbon"
	};
	chemical::element const my_carbon_basic{
		.number                    = 6, 
		.symbol                    = "C",
		.atomic_mass               = 12.0107*amu,
		.name                      = "Carbon"
	};
	chemical::element my_berkelium{
		.number = 97,
		.symbol = "Bk",
		.AR_electronegativity      = boost::nil,
		.covalent_radius           = boost::nil,
		.vdW_radius                = boost::nil,
		.max_bond_valence          = 6,
		.atomic_mass               = 247.07*amu,
		.Pauling_electronegativity = 1.30,
		.ionization_potential      = 6.1979*eV,
		.electron_affinity         = boost::nil,
		.red = 0.54, .green = 0.31, .blue = 0.89, 
		.name = "Berkelium"
	};

	cout << my_carbon << '\n';
	cout << my_berkelium <<'\n';

	cout<< my_berkelium.covalent_radius.value() <<'\n';

	boost::archive::yml_oarchive oa{std::cout, boost::archive::no_header};
	oa<<BOOST_SERIALIZATION_NVP(my_carbon);
}

#endif
#endif

